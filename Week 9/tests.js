defineTests(function (test) {
    var createPosition = ReferenceImplementations.createPosition;
    var createGrid = ReferenceImplementations.createGrid;

    function setMatrixFormatter(builder)
    {
        var F = formatters;
        var P = predicates;

        var formatter = F.byType( [ P.isMatrix, F.matrix ],
                                  [ P.any, F.simple ] );

        builder.setFormatter( formatter );
    }

    test(ReferenceImplementations.createPosition, function (builder) {
        builder.addInput(0, 0);
        builder.addInput(0, 1);
        builder.addInput(1, 0);
        builder.addInput(3, 4);
    });

    test(ReferenceImplementations.x, function (builder) {
        builder.addInput( createPosition(0, 0) );
        builder.addInput( createPosition(1, 2) );
        builder.addInput( createPosition(3, 1) );
    });

    test(ReferenceImplementations.y, function (builder) {
        builder.addInput( createPosition(0, 0) );
        builder.addInput( createPosition(1, 2) );
        builder.addInput( createPosition(3, 1) );
    });

    test(ReferenceImplementations.add, function (builder) {
        builder.addInput( createPosition(0, 0), createPosition(0, 0) );
        builder.addInput( createPosition(0, 0), createPosition(1, 0) );
        builder.addInput( createPosition(0, 0), createPosition(0, 1) );
        builder.addInput( createPosition(1, 0), createPosition(0, 0) );
        builder.addInput( createPosition(0, 1), createPosition(0, 0) );
        builder.addInput( createPosition(1, 2), createPosition(3, 4) );
    });

    test(ReferenceImplementations.north, function (builder) {
        builder.addInput();
    });

    test(ReferenceImplementations.east, function (builder) {
        builder.addInput();
    });

    test(ReferenceImplementations.south, function (builder) {
        builder.addInput();
    });

    test(ReferenceImplementations.west, function (builder) {
        builder.addInput();
    });

    test(ReferenceImplementations.around, function (builder) {
        builder.addInput( createPosition(0, 0) );
        builder.addInput( createPosition(1, 0) );
        builder.addInput( createPosition(3, 5) );
        builder.addInput( createPosition(-3, -2) );

        builder.setValidator(validators.io( equality.deep, equality.permutation(equality.deep) ));
    });

    test(ReferenceImplementations.createGrid, function (builder) {
        builder.addInput(1, 1, 0);
        builder.addInput(1, 2, 1);
        builder.addInput(2, 1, 2);
        builder.addInput(2, 2, 3);

        setMatrixFormatter(builder);
    });

    test(ReferenceImplementations.width, function (builder) {
        builder.addInput(createGrid(1,1,0));
        builder.addInput(createGrid(2,1,0));
        builder.addInput(createGrid(1,2,0));
        builder.addInput(createGrid(2,2,0));
        builder.addInput(createGrid(3,5,0));

        setMatrixFormatter(builder);
    });

    test(ReferenceImplementations.height, function (builder) {
        builder.addInput(createGrid(1,1,0));
        builder.addInput(createGrid(2,1,0));
        builder.addInput(createGrid(1,2,0));
        builder.addInput(createGrid(2,2,0));
        builder.addInput(createGrid(3,5,0));

        setMatrixFormatter(builder);
    });

    test(ReferenceImplementations.area, function (builder) {
        builder.addInput(createGrid(1,1,0));
        builder.addInput(createGrid(2,1,0));
        builder.addInput(createGrid(1,2,0));
        builder.addInput(createGrid(2,2,0));
        builder.addInput(createGrid(3,5,0));

        setMatrixFormatter(builder);
    });

    test(ReferenceImplementations.at, function (builder) {
        builder.addInput([[1]], createPosition(0,0));
        builder.addInput([[1,2]], createPosition(0,0));
        builder.addInput([[1,2]], createPosition(0,1));
        builder.addInput([[1],[2]], createPosition(0,0));
        builder.addInput([[1],[2]], createPosition(1,0));
        builder.addInput([[1,3],[2,4]], createPosition(0,0));
        builder.addInput([[1,3],[2,4]], createPosition(1,0));
        builder.addInput([[1,3],[2,4]], createPosition(0,1));
        builder.addInput([[1,3],[2,4]], createPosition(1,1));

        setMatrixFormatter(builder);
    });

    test(ReferenceImplementations.setAt, function (builder) {
        builder.addInput([[1]], createPosition(0,0), 5);
        builder.addInput([[1,2]], createPosition(0,0), 5);
        builder.addInput([[1,2]], createPosition(0,1), 5);
        builder.addInput([[1],[2]], createPosition(0,0), 5);
        builder.addInput([[1],[2]], createPosition(1,0), 5);
        builder.addInput([[1,3],[2,4]], createPosition(0,0), 4);
        builder.addInput([[1,3],[2,4]], createPosition(1,0), 9);
        builder.addInput([[1,3],[2,4]], createPosition(0,1), 1);
        builder.addInput([[1,3],[2,4]], createPosition(1,1), 2);

        setMatrixFormatter(builder);
    });

    test(ReferenceImplementations.count, function (builder) {
        builder.addInput([[1]], 0);
        builder.addInput([[1]], 1);
        builder.addInput([[1,2,3],[4,5,6]], 1);
        builder.addInput([[1,2,3],[4,5,6]], 4);
        builder.addInput([[1,2,1],[4,1,6]], 1);

        setMatrixFormatter(builder);
    });

    test(ReferenceImplementations.empty, function (builder) {
        builder.addInput();
    });

    test(ReferenceImplementations.junk, function (builder) {
        builder.addInput();
    });

    test(ReferenceImplementations.art, function (builder) {
        builder.addInput();
    });

    test(ReferenceImplementations.food, function (builder) {
        builder.addInput();
    });

    test(ReferenceImplementations.createMarket, function (builder) {
        builder.addInput(1, 1);
        builder.addInput(1, 2);
        builder.addInput(2, 1);
        builder.addInput(3, 4);

        setMatrixFormatter(builder);
    });

    test(ReferenceImplementations.standCounts, function (builder) {
        builder.addInput( [[0]] );
        builder.addInput( [[1]] );
        builder.addInput( [[2]] );
        builder.addInput( [[3]] );
        builder.addInput( [[0],[1],[2],[3]] );
        builder.addInput( [[0,3],[1,2],[2,1],[3,0]] );

        setMatrixFormatter(builder);
    });

    test(ReferenceImplementations.checkDistribution, function (builder) {
        builder.addInput( [[0]] );
        builder.addInput( [[1]] );
        builder.addInput( [[2]] );
        builder.addInput( [[3]] );
        builder.addInput( [[0],[1],[2],[3]] );
        builder.addInput( [[0,1,1,2,3]] );
        builder.addInput( [[0,1,2,3],[0,1,2,3]] );
        builder.addInput( [[0,0,0,0],[0,0,0,0]] );
        builder.addInput( [[0,1,0,1],[0,0,0,0]] );
        builder.addInput( [[0,1,0,0],[0,0,1,0]] );
        builder.addInput( [[0,1,0,0],[0,1,0,0]] );
        builder.addInput( [[0,1,0,0],[0,0,0,0],[0,1,0,0]] );
        builder.addInput( [[0,1,0,2],[0,0,0,0],[0,1,0,2]] );
        builder.addInput( [[0,1,0,2],[3,0,3,0],[0,1,0,2]] );
        builder.addInput( [[0,1,4,2],[3,0,4,0],[0,1,0,2]] );

        setMatrixFormatter(builder);
    });

    test(ReferenceImplementations.zigzag, function (builder) {
        builder.addInput( createGrid(1, 1, 0) );
        builder.addInput( createGrid(2, 1, 0) );
        builder.addInput( createGrid(1, 2, 0) );
        builder.addInput( createGrid(2, 2, 0) );
        builder.addInput( createGrid(3, 3, 0) );
        builder.addInput( createGrid(4, 4, 0) );

        setMatrixFormatter(builder);
    });

    test(ReferenceImplementations.swap, function (builder) {
        builder.addInput( [[0],[1]], createPosition(1,0), createPosition(0,0) );
        builder.addInput( [[1],[2]], createPosition(1,0), createPosition(0,0) );
        builder.addInput( [[1,2],[3,4]], createPosition(1,0), createPosition(0,0) );
        builder.addInput( [[1,2],[3,4]], createPosition(1,0), createPosition(0,1) );
        builder.addInput( [[1,2],[3,4]], createPosition(1,1), createPosition(0,0) );
        builder.addInput( [[1,2],[3,4]], createPosition(0,1), createPosition(0,0) );

        setMatrixFormatter(builder);
    });

    test(ReferenceImplementations.rearrange, function (builder) {
        builder.addInput( [[0]] );
        builder.addInput( [[0],[0]] );
        builder.addInput( [[0],[0],[0]] );
        builder.addInput( [[0],[1],[0]] );
        builder.addInput( [[0],[0],[1]] );
        builder.addInput( [[0],[1],[1]] );
        builder.addInput( [[0],[1],[2]] );
        builder.addInput( [[0],[2],[1]] );
        builder.addInput( [[0,0,0]] );
        builder.addInput( [[1,0,0]] );
        builder.addInput( [[0,1,0]] );
        builder.addInput( [[0,0,1]] );
        builder.addInput( [[0,1,1]] );
        builder.addInput( [[0,1,2],[2,3,1],[0,2,2]] );

        setMatrixFormatter(builder);
    });
});
