/*
  Schrijf je code hier
*/
function createPosition(x, y){
	return new Array(x,y);
}
function x(position){
	return position[0];
}
function y(position){
	return position[1];
}
function add(p, q){
	return createPosition(p[0]+q[0], p[1]+q[1]);
}
function north(){
	return createPosition(0,-1);
}
function south(){
	return createPosition(0,1);
}
function east(){
	return createPosition(1,0);
}
function west(){
	return createPosition(-1,0);
}
function around(p){
	return new Array(add(p,north),add(p,east),add(p,south),add(p,west));
	/* returns NaN, what do?*/
}
function createGrid(width, height, init){
	var grid = new Array(width);
	for (var x = 0; x<width; x++){
		grid[x] = new Array(height);
		for (var y = 0; y<grid[x].length; y++){
			grid[x][y] = init;
		}
	}
	return grid;
}
function width(grid){
	return grid.length;
}
function height(grid){
var height = -1;
	for (var x = 0; x<grid.length; x++){
		if (grid[x].length>height) height=grid[x].length;
	}
	return height;
}
function area(grid){
return height(grid)*width(grid);
}
function at(grid, position){
 return grid[x(position)][y(position)];
}
function setAt(grid, position, item){
	grid[x(position)][y(position)] = item;
}
function count(grid, item){
	var count = 0;
	for (var x = 0; x < grid.length; x++){
		for(var y = 0; y < grid[0].length; y++){
			if ( grid[x][y] === item) count++;
		}
	}
	return count;
}
function empty(){
	return 0;
}
function junk(){
	return 1;
}
function art(){
	return 2;
}
function food(){
	return 3;
}
function createMarket(size){
	return createGrid(size, size, empty());
}
function standCounts(market){
	return new Array(count(market, empty()), count(market, junk()), count(market, art()), count(market, food()));
}
function checkDistribution(market){
	for (var x = 0; x < market.length; x++){
		for(var y = 0; y < market[0].length; y++){
			for (var z = 0; z < around(market[x][y]).length; z++){
				if (around(market[x][y])[z] != null) if ( market[x][y] === around(market[x][y])[z])  return false;
			}
		}
	}
	return true;
}
function swap(market, from, to){
	var temp = market[from[0]][from[1]];
	market[from[0]][from[1]] = market[to[0]][to[1]];
	market[to[0]][to[1]] = temp;
}