var ReferenceImplementations;
(function (ReferenceImplementations) {
    function createPosition(x, y) {
        return [x, y];
    }
    ReferenceImplementations.createPosition = createPosition;

    function x(pos) {
        return pos[0];
    }
    ReferenceImplementations.x = x;

    function y(pos) {
        return pos[1];
    }
    ReferenceImplementations.y = y;

    function add(p, q) {
        return createPosition(x(p) + x(q), y(p) + y(q));
    }
    ReferenceImplementations.add = add;

    function north() {
        return createPosition(0, -1);
    }
    ReferenceImplementations.north = north;

    function east() {
        return createPosition(1, 0);
    }
    ReferenceImplementations.east = east;

    function west() {
        return createPosition(-1, 0);
    }
    ReferenceImplementations.west = west;

    function south() {
        return createPosition(0, 1);
    }
    ReferenceImplementations.south = south;

    function around(p) {
        return [
            add(p, north()),
            add(p, east()),
            add(p, south()),
            add(p, west())];
    }
    ReferenceImplementations.around = around;

    function createGrid(width, height, init) {
        var result = new Array(width);

        for (var i = 0; i !== width; ++i) {
            result[i] = new Array(height);

            for (var j = 0; j !== height; ++j) {
                result[i][j] = init;
            }
        }

        return result;
    }
    ReferenceImplementations.createGrid = createGrid;

    function width(grid) {
        return grid.length;
    }
    ReferenceImplementations.width = width;

    function height(grid) {
        return grid[0].length;
    }
    ReferenceImplementations.height = height;

    function area(grid) {
        return width(grid) * height(grid);
    }
    ReferenceImplementations.area = area;

    function isInside(grid, position) {
        return 0 <= x(position) && x(position) < width(grid) && 0 <= y(position) && y(position) < height(grid);
    }
    ReferenceImplementations.isInside = isInside;

    function at(grid, position) {
        return grid[x(position)][y(position)];
    }
    ReferenceImplementations.at = at;

    function setAt(grid, position, item) {
        grid[x(position)][y(position)] = item;
    }
    ReferenceImplementations.setAt = setAt;

    function count(grid, item) {
        var result = 0;

        for (var x = 0; x !== width(grid); ++x) {
            for (var y = 0; y !== height(grid); ++y) {
                var position = createPosition(x, y);

                if (at(grid, position) === item) {
                    ++result;
                }
            }
        }

        return result;
    }
    ReferenceImplementations.count = count;

    function empty() {
        return 0;
    }
    ReferenceImplementations.empty = empty;

    function junk() {
        return 1;
    }
    ReferenceImplementations.junk = junk;

    function art() {
        return 2;
    }
    ReferenceImplementations.art = art;

    function food() {
        return 3;
    }
    ReferenceImplementations.food = food;

    function createMarket(size) {
        return createGrid(size, size, empty());
    }
    ReferenceImplementations.createMarket = createMarket;

    function standCounts(market) {
        var junkCount = count(market, junk());
        var artCount = count(market, art());
        var foodCount = count(market, food());
        var free = area(market) - junkCount - artCount - foodCount;

        return [free, junkCount, artCount, foodCount];
    }
    ReferenceImplementations.standCounts = standCounts;

    function checkDistribution(market) {
        for (var x = 0; x !== width(market); ++x) {
            for (var y = 0; y !== height(market); ++y) {
                var position = createPosition(x, y);
                var centerStand = at(market, position);

                if (centerStand != empty()) {
                    var neighbors = around(position);

                    for (var i = 0; i !== neighbors.length; ++i) {
                        var neighborPosition = neighbors[i];

                        if (isInside(market, neighborPosition)) {
                            var neighborStand = at(market, neighborPosition);

                            if (neighborStand === centerStand) {
                                return false;
                            }
                        }
                    }
                }
            }
        }

        return true;
    }
    ReferenceImplementations.checkDistribution = checkDistribution;

    function zigzag(market) {
        var result = [];
        var x = 0;
        var dx = 1;
        var position;

        for (var y = 0; y !== height(market); ++y) {
            position = createPosition(x, y);

            while (isInside(market, position)) {
                result.push(position);
                x += dx;
                position = createPosition(x, y);
            }

            x -= dx;
            dx = -dx;
        }

        return result;
    }
    ReferenceImplementations.zigzag = zigzag;

    function swap(market, from, to) {
        var standAtFrom = at(market, from);
        var standAtTo = at(market, to);

        setAt(market, to, standAtFrom);
        setAt(market, from, standAtTo);
    }
    ReferenceImplementations.swap = swap;

    function rearrange(market) {
        var path = zigzag(market);
        var i = 0;
        var j = 0;

        while (i < path.length && j < path.length) {
            while (i < path.length && at(market, path[i]) !== empty()) {
                ++i;
            }

            while (j < path.length && at(market, path[j]) === empty()) {
                ++j;
            }

            if (i < path.length && j < path.length) {
                swap(market, path[j], path[i]);
            }
        }
    }
    ReferenceImplementations.rearrange = rearrange;
})(ReferenceImplementations || (ReferenceImplementations = {}));
