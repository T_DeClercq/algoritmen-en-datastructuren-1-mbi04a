/*
  Schrijf je code hier
*/
function arrayLengths(xss) {

var returnArray = new Array(xss.length);
for (var x = 0; x<xss.length; x++){
	returnArray[x] = xss[x].length;
}
return returnArray;

}
function isRectangular(xss){
var width = xss[0].length;
var bool = true;
for (var x = 0; x<xss.length; x++){
	if (xss[x].length != width) bool=false;
}
return bool;
}
function isSquare(xss){
var width = xss.length;
var bool = true;
for (var x = 0; x<xss.length; x++){
	if (xss[x].length != width) bool=false;
}
return bool;
}
function createGrid(width, height, value){
	var returnArray = new Array(width);
	for (var x = 0; x<width; x++){
		returnArray[x] = new Array(height);
		for (var y = 0; y<returnArray[x].length; y++){
			returnArray[x][y] = value;
		}
	}
	return returnArray;
}
function width(xss){
	return xss.length;
}
function height(xss){
var height = -1;
	for (var x = 0; x<xss.length; x++){
		if (xss[x].length>height) height=xss[x].length;
	}
	return height;
}
function zigZag(width, height){
	var returnArray = new Array(width);
	for (var x = 0; x<width; x++){
		returnArray[x] = new Array(height);
	}
	var value = 0;
	for (var y = 0; y<height; y++){
		for (var x = 0; x<width; x++){
			returnArray[x][y] = value;
			value++;
		}
	}
	return returnArray;

}
function getRow(xss, row){
	var returnArray = new Array(xss.length);
	for (var x = 0; x<returnArray.length; x++){
		returnArray[x] = xss[x][row];
	}
	return returnArray;
}
function getColumn(xss, column){
	var returnArray = new Array(xss[column].length);
	for (var x = 0; x<returnArray.length; x++){
		returnArray[x] = xss[column][x];
	}
	return returnArray;
}
function rowSums(xss){
	var returnArray = new Array(height(xss));
	for (var x = 0; x<returnArray.length; x++){
		var sum = 0;
		for (var y = 0; y<xss.length; y++){
			sum = sum + xss[y][x];
		}
		returnArray[x] = sum;
	}
	return returnArray;
}
/**
function columnSums(xss){
	var returnArray = new Array(width(xss));
	for (var x = 0; x<returnArray.length; x++){
		var sum = 0;
		for (var y = 0; y<xss[x].length; y++){
		console.log(xss[x].length);
			sum += xss[x][y];
			console.log(sum);
		}
	}
	return returnArray;
}
**/
function columnSums(xss){
		var returnArray = new Array(width(xss));
	for (var x = 0; x<returnArray.length; x++){
		var sum = 0;
		for (var y = 0; y<xss[x].length; y++){
			sum = sum + xss[x][y];
		}
		returnArray[x] = sum;
	}
	return returnArray;
}
function matrixAddition(xss, yss){
	if (width(xss) != width(yss)) return -1;
	if (height(xss) != height(yss)) return -1;
	var returnArray = new Array(width(xss));
	for (var x = 0; x<returnArray.length; x++){
		returnArray[x] = new Array(height(xss));
	}
	for (var x = 0; x<returnArray.length; x++){
	 for (var y = 0; y<returnArray[x].length; y++){
		returnArray[x][y] = xss[x][y] + yss[x][y];
	 }
	}
	return returnArray;
}