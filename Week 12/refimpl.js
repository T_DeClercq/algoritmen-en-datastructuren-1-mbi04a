var ReferenceImplementations;
(function (ReferenceImplementations) {
    function createArray(n, x) {
        var xs = new Array(n);

        for (var i = 0; i !== xs.length; ++i) {
            xs[i] = x;
        }

        return xs;
    }
    ReferenceImplementations.createArray = createArray;

    function createGrid(width, height, init) {
        var grid = new Array(width);

        for (var i = 0; i !== grid.length; ++i) {
            grid[i] = createArray(height, init);
        }

        return grid;
    }
    ReferenceImplementations.createGrid = createGrid;

    function width(grid) {
        return grid.length;
    }
    ReferenceImplementations.width = width;

    function height(grid) {
        return grid[0].length;
    }
    ReferenceImplementations.height = height;

    function at(grid, x, y) {
        return grid[x][y];
    }
    ReferenceImplementations.at = at;

    function row(grid, y) {
        var result = new Array(width(grid));

        for (var x = 0; x !== result.length; ++x) {
            result[x] = at(grid, x, y);
        }

        return result;
    }
    ReferenceImplementations.row = row;

    function column(grid, x) {
        var result = new Array(height(grid));

        for (var y = 0; y !== result.length; ++y) {
            result[y] = at(grid, x, y);
        }

        return result;
    }
    ReferenceImplementations.column = column;

    function dotProduct(xs, ys) {
        var total = 0;

        for (var i = 0; i !== xs.length; ++i) {
            total += xs[i] * ys[i];
        }

        return total;
    }
    ReferenceImplementations.dotProduct = dotProduct;

    function sum(xs) {
        var result = 0;

        for (var i = 0; i !== xs.length; ++i) {
            result += xs[i];
        }

        return result;
    }
    ReferenceImplementations.sum = sum;

    function average(xs) {
        return sum(xs) / xs.length;
    }
    ReferenceImplementations.average = average;

    function weightedAverage(xs, weights) {
        return dotProduct(xs, weights);
    }
    ReferenceImplementations.weightedAverage = weightedAverage;

    function frequencies(xs, from, to) {
        var result = createArray(to - from + 1, 0);

        for (var i = 0; i !== xs.length; ++i) {
            var k = xs[i];

            if (from <= k && k <= to) {
                result[k - from]++;
            }
        }

        return result;
    }
    ReferenceImplementations.frequencies = frequencies;

    function minimum(xs) {
        var result = xs[0];

        for (var i = 1; i < xs.length; ++i) {
            if (result > xs[i]) {
                result = xs[i];
            }
        }

        return result;
    }
    ReferenceImplementations.minimum = minimum;

    function maximum(xs) {
        var result = xs[0];

        for (var i = 1; i < xs.length; ++i) {
            if (result < xs[i]) {
                result = xs[i];
            }
        }

        return result;
    }
    ReferenceImplementations.maximum = maximum;

    function sort(xs) {
        var min = minimum(xs);
        var max = maximum(xs);
        var fs = frequencies(xs, min, max);
        var result = new Array(xs.length);

        var j = 0;
        for (var i = 0; i !== fs.length; ++i) {
            for (var k = 0; k !== fs[i]; ++k) {
                result[j] = min + i;
                ++j;
            }
        }

        return result;
    }
    ReferenceImplementations.sort = sort;

    function isEven(n) {
        return n % 2 === 0;
    }
    ReferenceImplementations.isEven = isEven;

    function median(xs) {
        var sorted = sort(xs);

        if (isEven(sorted.length)) {
            var x = sorted[sorted.length / 2 - 1];
            var y = sorted[sorted.length / 2];

            return (x + y) / 2;
        } else {
            return sorted[Math.floor(sorted.length / 2)];
        }
    }
    ReferenceImplementations.median = median;

    function maakScoreMatrix(nStudenten, nTests) {
        return createGrid(nStudenten, nTests, 0);
    }
    ReferenceImplementations.maakScoreMatrix = maakScoreMatrix;

    function score(m, student, test) {
        return at(m, student, test);
    }
    ReferenceImplementations.score = score;

    function aantalTests(m) {
        return height(m);
    }
    ReferenceImplementations.aantalTests = aantalTests;

    function aantalStudenten(m) {
        return width(m);
    }
    ReferenceImplementations.aantalStudenten = aantalStudenten;

    function resultatenVanStudent(m, student) {
        return column(m, student);
    }
    ReferenceImplementations.resultatenVanStudent = resultatenVanStudent;

    function resultatenVanTest(m, test) {
        return row(m, test);
    }
    ReferenceImplementations.resultatenVanTest = resultatenVanTest;

    function berekenEindresultaten(resultaten, gewichten) {
        var result = new Array(aantalStudenten(resultaten));

        for (var i = 0; i !== result.length; ++i) {
            result[i] = Math.ceil(weightedAverage(resultatenVanStudent(resultaten, i), gewichten) * 2);
        }

        return result;
    }
    ReferenceImplementations.berekenEindresultaten = berekenEindresultaten;

    function gemiddeldes(resultaten) {
        var result = new Array(aantalTests(resultaten));

        for (var i = 0; i !== result.length; ++i) {
            result[i] = average(resultatenVanTest(resultaten, i));
        }

        return result;
    }
    ReferenceImplementations.gemiddeldes = gemiddeldes;

    function histogram(resultaten) {
        var result = new Array(aantalTests(resultaten));

        for (var i = 0; i !== result.length; ++i) {
            result[i] = frequencies(resultatenVanTest(resultaten, i), 0, 10);
        }

        return result;
    }
    ReferenceImplementations.histogram = histogram;

    function medianen(resultaten) {
        var result = new Array(aantalTests(resultaten));

        for (var i = 0; i !== result.length; ++i) {
            result[i] = median(resultatenVanTest(resultaten, i));
        }

        return result;
    }
    ReferenceImplementations.medianen = medianen;
})(ReferenceImplementations || (ReferenceImplementations = {}));
