/*
  Schrijf je code hier
*/
function createArray(n, init){
	var arr = new Array(n)
	for(var i=0;i<arr.length;i++){
		arr[i] = init;
	}
	return arr;
}
function createGrid(width, height, init){
	var grid = createArray(width, createArray(height, init))
	return grid
}
function width(grid){
	return grid.length
}
function height(grid){
	return grid[0].length
}
function at(grid, x, y){
	if (width(grid)<x || height(grid)<y) return null 
	return grid[x][y]
}
function column(grid,x){
	if (width(grid)<x || x<0) return null
	return grid[x]
}
function row(grid, y){
	if (height(grid)<y || y<0) return null
	else {
		var row = new Array(width(grid))
		for (var x = 0; x<row.length; x++){
			row[x] = grid[x][y]
		}
	return row	
	}
}
function sum(xs){
	var res = 0;
	for (var x = 0; x<xs.length; x++){
		res+=xs[x]
	}
	return res
}
function average(xs){
	return sum(xs)/xs.length
}
function dotProduct(xs,ys){
	if (xs.length != ys.length) return null
	var res = 0
	for (var x = 0; x<xs.length; x++){
		res+=xs[x]*ys[x]
	}
	return res
}
function weightedAverage(xs, weights){
	if (xs.length != weights.length) return null
	//eigenlijk overbodige check gezien oproeping dotProduct
	return dotProduct(xs, weights)
}
function frequencies(xs, from, to){
	var res = createArray(to-from+1, 0)
	for (var x = 0; x<xs.length; x++){
		if (!(xs[x]>to || xs[x]<from)){
			res[xs[x]-from]++
		}
	}
	return res
}
function minimum(xs){
	var min = 999
	for (var x = 0; x<xs.length; x++){
		if (xs[x]<min) min = xs[x]
	}
	return min
	
}
function maximum(xs){
	var max = 0
	for (var x = 0; x<xs.length; x++){
		if (xs[x]>max) max = xs[x]
	}
	return max
}
function sort(xs){
	var res = createArray(xs.length, 0)
	var freq = frequencies(xs, minimum(xs), maximum(xs))
	var x = 0
	for (var pos = 0; pos<freq.length; pos++){
		for (var amt = 0; amt < freq[pos]; amt++){
			res[x] = minimum(xs)+pos
			x++
		}
	}
	return res
}
function median(xs){
	if ((xs.length-1)%2 != 0){
		return (sort(xs)[((xs.length-1)/2)-0.5]+ sort(xs)[((xs.length-1)/2)+0.5])/2
	}
	else return sort(xs)[(xs.length-1)/2]
}
function maakScoreMatrix(nStudenten, nTests){
	return createGrid(nStudenten, nTests, 0)
}
function score(scoreMatrix, student, test){
	return at(scoreMatrix, student, test)
}
function aantalTests(scoreMatrix){
	return height(scoreMatrix)
}
function aantalStudenten(scoreMatrix){
	return width(scoreMatrix)
}
function resultatenVanStudent(scoreMatrix, student){
	return column(scoreMatrix, student)
}
function resultatenVanTest(scoreMatrix, test){
	return row(scoreMatrix, test)
}
function berekenEindresultaten(scoreMatrix, gewichten){
	var res = createArray(aantalStudenten(scoreMatrix), 0)
	for (var student = 0; student < res.length; student++){
		res[student] = Math.ceil(weightedAverage(resultatenVanStudent(scoreMatrix,student), gewichten)*2)
	}
	return res
}
function gemiddeldes(scoreMatrix){
	var res = createArray(aantalTests(scoreMatrix),0)
	for (var test = 0; test < res.length; test++){
		res[test] = average(resultatenVanTest(scoreMatrix, test))
	}
	return res
}
function histogram(scoreMatrix){
	var res = createArray(aantalTests(scoreMatrix), 0)
	for (var test = 0; test < res.length; test++){
		res[test] = frequencies(resultatenVanTest(scoreMatrix, test), 0, 10)
	}
	return res
}
function medianen(scoreMatrix){
	var res = createArray(aantalTests(scoreMatrix), 0)
	for (var test = 0; test < res.length; test++){
		res[test] = median(resultatenVanTest(scoreMatrix, test))
	}
	return res
}