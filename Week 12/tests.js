defineTests(function (test) {
    function withCustomFormatters( builder, func )
    {
        var F = formatters;
        var P = predicates;

        builder.setFormatter( F.byType( [ isClassroom, classroomFormatter ],
                                        [ P.isMatrix, F.matrix ],
                                        [ P.any, F.simple ] ) );

        func( builder );
    }

    function withClassroomFormatter( builder, func )
    {
        var F = formatters;
        var P = predicates;

        builder.setFormatter( F.byType( [ isClassroom, classroomFormatter ],
                                        [ P.any, F.simple ] ) );

        func( builder );
    }

    function isClassroom(x)
    {
        return predicates.isArray(x) &&
            x.length == 2 &&
            predicates.isArray( x[0] ) &&
            predicates.isMatrix( x[1] );
    }

    function classroomFormatter(cr)
    {
        var table = newElement('table');

        var tbody = newElement('tbody');
        table.append(tbody);

        var row = newElement('tr');
        tbody.append(row);

        var d1 = newElement('td');
        var d2 = newElement('td');
        d1.append( formatters.matrix( [ cr[0] ] ) );
        d2.append( formatters.matrix( cr[1] ) );

        row.append(d1);
        row.append(d2);

        return table;
    }

    test(ReferenceImplementations.createArray, function (builder) {
        builder.addInput( 0, 0 );
        builder.addInput( 0, 1 );
        builder.addInput( 1, 0 );
        builder.addInput( 5, 3 );
    });

    test(ReferenceImplementations.createGrid, function (builder) {
        withCustomFormatters( builder, function (builder) {
            builder.addInput( 1, 1, 0 );
            builder.addInput( 2, 1, 1 );
            builder.addInput( 1, 2, 2 );
            builder.addInput( 5, 3, 3 );
        } );
    });

    test(ReferenceImplementations.width, function (builder) {
        withCustomFormatters( builder, function (builder) {
            function grid(w, h) {
                return ReferenceImplementations.createGrid(w, h, 0);
            };

            builder.addInput( grid(1, 1) );
            builder.addInput( grid(1, 2) );
            builder.addInput( grid(3, 1) );
            builder.addInput( grid(2, 3) );
        } );
    });

    test(ReferenceImplementations.height, function (builder) {
        withCustomFormatters( builder, function (builder) {
            function grid(w, h) {
                return ReferenceImplementations.createGrid(w, h, 0);
            };

            builder.addInput( grid(1, 1) );
            builder.addInput( grid(1, 2) );
            builder.addInput( grid(3, 1) );
            builder.addInput( grid(2, 3) );
        } );
    });

    test(ReferenceImplementations.at, function (builder) {
        withCustomFormatters( builder, function (builder) {
            var grid = [ [ 1, 2, 3 ], [ 4, 5, 6 ] ];

            builder.addInput( [[0]], 0, 0 );
            builder.addInput( [[0,1]], 0, 1 );
            builder.addInput( [[0],[1]], 1, 0 );
            builder.addInput( grid, 0, 0 );
            builder.addInput( grid, 1, 0 );
            builder.addInput( grid, 0, 1 );
            builder.addInput( grid, 1, 1 );
            builder.addInput( grid, 0, 2 );
            builder.addInput( grid, 1, 2 );
        } );
    });

    test(ReferenceImplementations.column, function (builder) {
        withCustomFormatters( builder, function (builder) {
            builder.addInput( [[0]], 0 );
            builder.addInput( [[0,1]], 0 );
            builder.addInput( [[0,1],[2,3]], 0 );
            builder.addInput( [[0,1],[2,3]], 1 );
        } );
    });

    test(ReferenceImplementations.row, function (builder) {
        withCustomFormatters( builder, function (builder) {
            builder.addInput( [[0]], 0 );
            builder.addInput( [[0,1]], 0 );
            builder.addInput( [[0,1]], 1 );
            builder.addInput( [[0,1],[2,3]], 0 );
            builder.addInput( [[0,1],[2,3]], 1 );
        } );
    });

    test(ReferenceImplementations.dotProduct, function (builder) {
        builder.addInput( [], [] );
        builder.addInput( [0], [0] );
        builder.addInput( [1], [0] );
        builder.addInput( [0], [1] );
        builder.addInput( [1], [1] );
        builder.addInput( [2], [2] );
        builder.addInput( [1,2,3], [4,5,6] );
    });

    test(ReferenceImplementations.sum, function (builder) {
        builder.addInput( [] );
        builder.addInput( [0] );
        builder.addInput( [1] );
        builder.addInput( [1,2] );
        builder.addInput( [1,2,3] );
    });

    test(ReferenceImplementations.average, function (builder) {
        builder.addInput( [0] );
        builder.addInput( [1] );
        builder.addInput( [1,2] );
        builder.addInput( [1,2,3] );
    });

    test(ReferenceImplementations.weightedAverage, function (builder) {
        builder.addInput( [0], [1] );
        builder.addInput( [1], [1] );
        builder.addInput( [1,2], [.5,.5] );
        builder.addInput( [1,2], [.25,.75] );
        builder.addInput( [1,2], [0,1] );
        builder.addInput( [1,2,3], [.2,.2,.6] );
        builder.addInput( [1,2,3], [.2,.5,.3] );
    });

    test(ReferenceImplementations.frequencies, function (builder) {
        builder.addInput( [0], 0, 0 );
        builder.addInput( [0], 0, 1 );
        builder.addInput( [0], 0, 5 );
        builder.addInput( [0], 1, 5 );
        builder.addInput( [0,1,2,3,4,5,6], 1, 5 );
        builder.addInput( [0,1,2,2,3,3,3,4], 1, 3 );
    });

    test(ReferenceImplementations.minimum, function (builder) {
        builder.addInput( [0] );
        builder.addInput( [0,1] );
        builder.addInput( [0,1,2] );
        builder.addInput( [2,1,0] );
        builder.addInput( [3,8,6] );
        builder.addInput( [7,5,9] );
        builder.addInput( [5,7,9,1,3,2,4,6,8] );
    });

    test(ReferenceImplementations.maximum, function (builder) {
        builder.addInput( [0] );
        builder.addInput( [0,1] );
        builder.addInput( [0,1,2] );
        builder.addInput( [2,1,0] );
        builder.addInput( [3,8,6] );
        builder.addInput( [7,5,9] );
        builder.addInput( [5,7,9,1,3,2,4,6,8] );
    });

    test(ReferenceImplementations.sort, function (builder) {
        builder.addInput( [0] );
        builder.addInput( [0,1] );
        builder.addInput( [0,1,2] );
        builder.addInput( [2,1,0] );
        builder.addInput( [3,8,6] );
        builder.addInput( [7,5,9] );
        builder.addInput( [5,7,9,1,3,2,4,6,8] );
    });

    test(ReferenceImplementations.median, function (builder) {
        builder.addInput( [0] );
        builder.addInput( [0,1] );
        builder.addInput( [0,1,2] );
        builder.addInput( [2,1,0] );
        builder.addInput( [2,1,0,3] );
        builder.addInput( [2,0,1,3] );
        builder.addInput( [2,3,1,0] );
        builder.addInput( [3,8,6] );
        builder.addInput( [7,5,9] );
        builder.addInput( [5,7,9,1,3,2,4,6,8] );
    });

    test(ReferenceImplementations.maakScoreMatrix, function (builder) {
        withCustomFormatters( builder, function (builder) {
            builder.addInput( 1, 1 );
            builder.addInput( 1, 2 );
            builder.addInput( 2, 1 );
            builder.addInput( 3, 4 );
        } );
    });

    test(ReferenceImplementations.score, function (builder) {
        withCustomFormatters( builder, function (builder) {
            builder.addInput( [[1]], 0, 0 );
            builder.addInput( [[1,2],[3,4]], 0, 0 );
            builder.addInput( [[1,2],[3,4]], 0, 1 );
            builder.addInput( [[1,2],[3,4]], 1, 0 );
            builder.addInput( [[1,2],[3,4]], 1, 1 );
        } );
    });

    test(ReferenceImplementations.aantalTests, function (builder) {
        withCustomFormatters( builder, function (builder) {
            builder.addInput( [[1]], 0, 0 );
            builder.addInput( [[1,2,3,4]], 0, 0 );
            builder.addInput( [[1],[2],[3],[4]], 0, 0 );
            builder.addInput( [[1,2],[3,4]] );
            builder.addInput( [[1,2,5],[3,4,6]] );
        } );
    });

    test(ReferenceImplementations.aantalStudenten, function (builder) {
        withCustomFormatters( builder, function (builder) {
            builder.addInput( [[1]] );
            builder.addInput( [[1,2,3,4]] );
            builder.addInput( [[1],[2],[3],[4]] );
            builder.addInput( [[1,2],[3,4]] );
            builder.addInput( [[1,2,5],[3,4,6]] );
        } );
    });

    test(ReferenceImplementations.resultatenVanStudent, function (builder) {
        withCustomFormatters( builder, function (builder) {
            builder.addInput( [[1]], 0 );
            builder.addInput( [[1,2,3,4]], 0 );
            builder.addInput( [[1],[2],[3],[4]], 0 );
            builder.addInput( [[1],[2],[3],[4]], 1 );
            builder.addInput( [[1],[2],[3],[4]], 2 );
            builder.addInput( [[1],[2],[3],[4]], 3 );
            builder.addInput( [[1,2],[3,4]], 0 );
            builder.addInput( [[1,2],[3,4]], 1 );
            builder.addInput( [[1,2,5],[3,4,6]], 0 );
            builder.addInput( [[1,2,5],[3,4,6]], 1 );
        } );
    });

    test(ReferenceImplementations.resultatenVanTest, function (builder) {
        withCustomFormatters( builder, function (builder) {
            builder.addInput( [[1]], 0 );
            builder.addInput( [[1,2,3,4]], 0 );
            builder.addInput( [[1,2,3,4]], 1 );
            builder.addInput( [[1,2,3,4]], 2 );
            builder.addInput( [[1,2,3,4]], 3 );
            builder.addInput( [[1],[2],[3],[4]], 0 );
            builder.addInput( [[1,2],[3,4]], 0 );
            builder.addInput( [[1,2],[3,4]], 1 );
            builder.addInput( [[1,2,5],[3,4,6]], 0 );
            builder.addInput( [[1,2,5],[3,4,6]], 1 );
        } );
    });

    test(ReferenceImplementations.berekenEindresultaten, function (builder) {
        withCustomFormatters( builder, function (builder) {
            builder.addInput( [[10],[5]], [1] );
            builder.addInput( [[9,8,7,5]], [.1,.2,.3,.4] );
            builder.addInput( [[10,8],[4,6]], [.5,.5] );
            builder.addInput( [[10,8],[4,6]], [.75,.25] );
            builder.addInput( [[8,7,8,9],[0,7,6,5]], [.25,.25,.25,.25] );
            builder.addInput( [[8,7,8,9],[0,7,6,5]], [.1,.2,.3,.4] );
            builder.addInput( [[8,7,8,9],[0,7,6,5],[9,8,7,0]], [.1,.2,.3,.4] );
        } );
    });

    test(ReferenceImplementations.gemiddeldes, function (builder) {
        withCustomFormatters( builder, function (builder) {
            builder.addInput( [[10]] );
            builder.addInput( [[10,10]] );
            builder.addInput( [[10,8]] );
            builder.addInput( [[10,8,6]] );
            builder.addInput( [[10],[8],[6]] );
            builder.addInput( [[10,5],[8,6],[6,7]] );

        } );
    });

    test(ReferenceImplementations.histogram, function (builder) {
        withCustomFormatters( builder, function (builder) {
            builder.addInput( [[0]] );
            builder.addInput( [[1]] );
            builder.addInput( [[2]] );
            builder.addInput( [[3]] );
            builder.addInput( [[4]] );
            builder.addInput( [[10]] );
            builder.addInput( [[10],[10]] );
            builder.addInput( [[10],[10],[10]] );
            builder.addInput( [[10],[8],[10]] );
            builder.addInput( [[10,8]] );
            builder.addInput( [[10,8,6],[10,6,10],[6,8,8]] );
        } );
    });

    test(ReferenceImplementations.medianen, function (builder) {
        withCustomFormatters( builder, function (builder) {
            builder.addInput( [[0]] );
            builder.addInput( [[1]] );
            builder.addInput( [[8]] );
            builder.addInput( [[1],[2],[3],[4]] );
            builder.addInput( [[1],[2],[3],[4],[5]] );
            builder.addInput( [[4],[1],[2],[3],[5]] );
            builder.addInput( [[10,8,6],[10,6,10],[6,3,8]] );
        } );
    });
});
