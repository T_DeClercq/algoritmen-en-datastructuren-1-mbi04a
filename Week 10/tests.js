defineTests(function (test) {
    function withCustomFormatters( builder, func )
    {
        var F = formatters;
        var P = predicates;

        builder.setFormatter( F.byType( [ isClassroom, classroomFormatter ],
                                        [ P.isMatrix, F.matrix ],
                                        [ P.any, F.simple ] ) );

        func( builder );
    }

    function withClassroomFormatter( builder, func )
    {
        var F = formatters;
        var P = predicates;

        builder.setFormatter( F.byType( [ isClassroom, classroomFormatter ],
                                        [ P.any, F.simple ] ) );

        func( builder );
    }

    function isClassroom(x)
    {
        return predicates.isArray(x) &&
            x.length == 2 &&
            predicates.isArray( x[0] ) &&
            predicates.isMatrix( x[1] );
    }

    function classroomFormatter(cr)
    {
        var table = newElement('table');

        var tbody = newElement('tbody');
        table.append(tbody);

        var row = newElement('tr');
        tbody.append(row);

        var d1 = newElement('td');
        var d2 = newElement('td');
        d1.append( formatters.matrix( [ cr[0] ] ) );
        d2.append( formatters.matrix( cr[1] ) );

        row.append(d1);
        row.append(d2);

        return table;
    }

    test(ReferenceImplementations.createPosition, function (builder) {
        builder.addInput(0, 0);
        builder.addInput(0, 1);
        builder.addInput(2, 0);
        builder.addInput(3, 4);
    });

    test(ReferenceImplementations.x, function (builder) {
        var pos = ReferenceImplementations.createPosition;

        builder.addInput( pos(0, 0) );
        builder.addInput( pos(0, 1) );
        builder.addInput( pos(2, 0) );
        builder.addInput( pos(3, 4) );
    });

    test(ReferenceImplementations.y, function (builder) {
        var pos = ReferenceImplementations.createPosition;

        builder.addInput( pos(0, 0) );
        builder.addInput( pos(0, 1) );
        builder.addInput( pos(2, 0) );
        builder.addInput( pos(3, 4) );
    });

    test(ReferenceImplementations.stringOfPosition, function (builder) {
        var pos = ReferenceImplementations.createPosition;

        builder.addInput( pos(0, 0) );
        builder.addInput( pos(0, 1) );
        builder.addInput( pos(2, 0) );
        builder.addInput( pos(3, 4) );
    });

    test(ReferenceImplementations.createArray, function (builder) {
        builder.addInput( 0, 0 );
        builder.addInput( 0, 1 );
        builder.addInput( 1, 0 );
        builder.addInput( 5, 3 );
    });

    test(ReferenceImplementations.createGrid, function (builder) {
        withCustomFormatters( builder, function (builder) {
            builder.addInput( 1, 1, 0 );
            builder.addInput( 2, 1, 1 );
            builder.addInput( 1, 2, 2 );
            builder.addInput( 5, 3, 3 );
        } );
    });

    test(ReferenceImplementations.width, function (builder) {
        withCustomFormatters( builder, function (builder) {
            function grid(w, h) {
                return ReferenceImplementations.createGrid(w, h, 0);
            };

            builder.addInput( grid(1, 1) );
            builder.addInput( grid(1, 2) );
            builder.addInput( grid(3, 1) );
            builder.addInput( grid(2, 3) );
        } );
    });

    test(ReferenceImplementations.height, function (builder) {
        withCustomFormatters( builder, function (builder) {
            function grid(w, h) {
                return ReferenceImplementations.createGrid(w, h, 0);
            };

            builder.addInput( grid(1, 1) );
            builder.addInput( grid(1, 2) );
            builder.addInput( grid(3, 1) );
            builder.addInput( grid(2, 3) );
        } );
    });

    test(ReferenceImplementations.at, function (builder) {
        withCustomFormatters( builder, function (builder) {
            var grid = [ [ 1, 2, 3 ], [ 4, 5, 6 ] ];
            var pos = ReferenceImplementations.createPosition;

            builder.addInput( [[0]], pos(0,0) );
            builder.addInput( [[0,1]], pos(0,1) );
            builder.addInput( [[0],[1]], pos(1,0) );
            builder.addInput( grid, pos(0,0) );
            builder.addInput( grid, pos(1,0) );
            builder.addInput( grid, pos(0,1) );
            builder.addInput( grid, pos(1,1) );
            builder.addInput( grid, pos(0,2) );
            builder.addInput( grid, pos(1,2) );
        } );
    });

    test(ReferenceImplementations.setAt, function (builder) {
        withCustomFormatters( builder, function (builder) {
            var grid = [ [ 1, 2, 3 ], [ 4, 5, 6 ] ];
            var pos = ReferenceImplementations.createPosition;

            builder.addInput( [[0]], pos(0,0), 9 );
            builder.addInput( [[0,1]], pos(0,1), 9 );
            builder.addInput( [[0],[1]], pos(1,0), 8 );
            builder.addInput( grid, pos(0,0), 7 );
            builder.addInput( grid, pos(1,0), 6 );
            builder.addInput( grid, pos(0,1), 5 );
            builder.addInput( grid, pos(1,1), 4 );
            builder.addInput( grid, pos(0,2), 3 );
            builder.addInput( grid, pos(1,2), 2 );
        } );
    });

    test(ReferenceImplementations.createCode, function (builder) {
        builder.addInput( 0, 0 );
        builder.addInput( 0, 1 );
        builder.addInput( 2, 0 );
        builder.addInput( 3, 4 );
    });

    test(ReferenceImplementations.row, function (builder) {
        var code = ReferenceImplementations.createCode;

        builder.addInput( code(0, 0) );
        builder.addInput( code(0, 2) );
        builder.addInput( code(5, 0) );
        builder.addInput( code(4, 9) );
    });

    test(ReferenceImplementations.index, function (builder) {
        var code = ReferenceImplementations.createCode;

        builder.addInput( code(0, 0) );
        builder.addInput( code(0, 2) );
        builder.addInput( code(5, 0) );
        builder.addInput( code(4, 9) );
    });

    test(ReferenceImplementations.index, function (builder) {
        var code = ReferenceImplementations.createCode;

        builder.addInput( code(0, 0) );
        builder.addInput( code(0, 2) );
        builder.addInput( code(5, 0) );
        builder.addInput( code(4, 9) );
    });
    
    test(ReferenceImplementations.stringOfCode, function (builder) {
        var code = ReferenceImplementations.createCode;

        builder.addInput( code(0, 0) );
        builder.addInput( code(0, 2) );
        builder.addInput( code(5, 0) );
        builder.addInput( code(4, 9) );
    });

    test(ReferenceImplementations.createClassroom, function (builder) {
        builder.addInput( 1, 1 );
        builder.addInput( 1, 2 );
        builder.addInput( 2, 1 );
        builder.addInput( 3, 3 );
    });

    test(ReferenceImplementations.rowCount, function (builder) {
        withCustomFormatters( builder, function (builder) {
            var cr = ReferenceImplementations.createClassroom;
        
            builder.addInput( cr(1, 1) );
            builder.addInput( cr(1, 2) );
            builder.addInput( cr(2, 1) );
            builder.addInput( cr(3, 3) );
        } );
    });

    test(ReferenceImplementations.computersPerRow, function (builder) {
        withCustomFormatters( builder, function (builder) {
            var cr = ReferenceImplementations.createClassroom;
            
            builder.addInput( cr(1, 1) );
            builder.addInput( cr(1, 2) );
            builder.addInput( cr(2, 1) );
            builder.addInput( cr(3, 3) );
        } );
    });

    test(ReferenceImplementations.allComputerCodes, function (builder) {
        withCustomFormatters( builder, function (builder) {
            var cr = ReferenceImplementations.createClassroom;
            
            builder.addInput( cr(1, 1) );
            builder.addInput( cr(1, 2) );
            builder.addInput( cr(2, 1) );
            builder.addInput( cr(3, 3) );
        } );
    });

    test(ReferenceImplementations.isPoweredRow, function (builder) {
        withCustomFormatters( builder, function (builder) {
            var t = true;
            var f = false;

            builder.addInput( [ [t,t,t], [[t,t,t],[t,t,t]] ], 0 );
            builder.addInput( [ [f,t,t], [[t,t,t],[t,t,t]] ], 0 );
            builder.addInput( [ [t,t,t], [[f,f,f],[t,t,t]] ], 0 );
            builder.addInput( [ [t,f,t], [[t,t,t],[t,t,t]] ], 0 );
            builder.addInput( [ [t,f,t], [[t,t,t],[t,t,t]] ], 1 );
            builder.addInput( [ [t,f,t], [[t,t,t],[t,t,t]] ], 2 );
            builder.addInput( [ [t,t,f], [[t,t,t],[t,t,t]] ], 2 );
        } );
    });

    test(ReferenceImplementations.computerPosition, function (builder) {
        var code = ReferenceImplementations.createCode;

        builder.addInput( code(0,0) );
        builder.addInput( code(1,0) );
        builder.addInput( code(0,2) );
        builder.addInput( code(3,4) );
    });

    test(ReferenceImplementations.isBroken, function (builder) {
        withCustomFormatters( builder, function (builder) {
            var code = ReferenceImplementations.createCode;
            var t = true;
            var f = false;
            
            for ( var y = 0; y != 3; ++y )
            {
                for ( var x = 0; x != 2; ++x )
                {
                    builder.addInput( [ [t,f,t], [[f,t,t],[t,f,t]] ], code(y, x) );
                }
            }
        } );
    });

    test(ReferenceImplementations.isOperational, function (builder) {
        withCustomFormatters( builder, function (builder) {
            var code = ReferenceImplementations.createCode;
            var t = true;
            var f = false;
            
            for ( var y = 0; y != 3; ++y )
            {
                for ( var x = 0; x != 2; ++x )
                {
                    builder.addInput( [ [t,f,t], [[f,t,t],[t,f,t]] ], code(y, x) );
                }
            }
        } );
    });

    test(ReferenceImplementations.setRowElectricity, function (builder) {
        withCustomFormatters( builder, function (builder) {
            var t = true;
            var f = false;
            
            for ( var y = 0; y != 3; ++y )
            {
                builder.addInput( [ [t,f,f], [[f,t,t],[t,f,t]] ], y, true );
                builder.addInput( [ [t,f,f], [[f,t,t],[t,f,t]] ], y, false );
            }
        } );
    });

    test(ReferenceImplementations.setComputerState, function (builder) {
        withCustomFormatters( builder, function (builder) {
            var code = ReferenceImplementations.createCode;
            var t = true;
            var f = false;
            
            for ( var y = 0; y != 3; ++y )
            {
                for ( var x = 0; x != 2; ++x )
                {
                    builder.addInput( [ [t,f,t], [[f,t,t],[t,f,t]] ], code(y, x), true );
                    builder.addInput( [ [t,f,t], [[f,t,t],[t,f,t]] ], code(y, x), false );
                }
            }
        } );
    });

    test(ReferenceImplementations.countOperational, function (builder) {
        withCustomFormatters( builder, function (builder) {
            var t = true;
            var f = false;
            
            builder.addInput( [ [t], [[f]] ] );
            builder.addInput( [ [t], [[t]] ] );
            builder.addInput( [ [f], [[t]] ] );
            builder.addInput( [ [t,f,f], [[f,t,t],[t,f,t]] ] );
            builder.addInput( [ [t,t,t], [[f,t,t],[t,f,t]] ] );
        } );
    });

    test(ReferenceImplementations.countBroken, function (builder) {
        withCustomFormatters( builder, function (builder) {
            var t = true;
            var f = false;
            
            builder.addInput( [ [t], [[f]] ] );
            builder.addInput( [ [t], [[t]] ] );
            builder.addInput( [ [f], [[t]] ] );
            builder.addInput( [ [t,f,f], [[f,t,t],[t,f,t]] ] );
            builder.addInput( [ [t,t,t], [[f,t,t],[t,f,t]] ] );
        } );
    });

    test(ReferenceImplementations.countUnpoweredWorking, function (builder) {
        withCustomFormatters( builder, function (builder) {
            var t = true;
            var f = false;
            
            builder.addInput( [ [t], [[f]] ] );
            builder.addInput( [ [t], [[t]] ] );
            builder.addInput( [ [f], [[t]] ] );
            builder.addInput( [ [t,f,f], [[f,t,t],[t,f,t]] ] );
            builder.addInput( [ [t,t,t], [[f,t,t],[t,f,t]] ] );
        } );
    });

    test(ReferenceImplementations.classifyComputers, function (builder) {
        withClassroomFormatter( builder, function (builder) {
            var t = true;
            var f = false;
            
            builder.addInput( [ [t], [[f]] ] );
            builder.addInput( [ [t], [[t]] ] );
            builder.addInput( [ [f], [[t]] ] );
            builder.addInput( [ [t,f,f], [[f,t,t],[t,f,t]] ] );
            builder.addInput( [ [t,t,t], [[f,t,t],[t,f,t]] ] );
        } );
    });

    test(ReferenceImplementations.findRearrangement, function (builder) {
        withClassroomFormatter( builder, function (builder) {
            var t = true;
            var f = false;
            
            builder.addInput( [ [t], [[t], [f]] ] );
            builder.addInput( [ [t], [[f], [t]] ] );
            builder.addInput( [ [t], [[f], [t], [t]] ] );
            builder.addInput( [ [t,f], [[f,t], [f,t], [f,t]] ] );
            builder.addInput( [ [t,t], [[f,t], [f,t], [f,t]] ] );
            builder.addInput( [ [f,f], [[f,t], [f,t], [f,t]] ] );
            builder.addInput( [ [t,f,f], [[f,t,t], [f,t,t], [f,t,t]] ] );
            builder.addInput( [ [t,f,f], [[f,t,f], [f,f,t], [f,t,t]] ] );
        } );
    });

    test(ReferenceImplementations.countExamReadyOnRow, function (builder) {
        withClassroomFormatter( builder, function (builder) {
            var t = true;
            var f = false;
            
            builder.addInput( [ [t], [[t], [f]] ], 0 );
            builder.addInput( [ [t], [[t], [t]] ], 0 );
            builder.addInput( [ [t], [[f], [t]] ], 0 );
            builder.addInput( [ [f], [[f], [t]] ], 0 );
            builder.addInput( [ [t], [[t], [t], [t]] ], 0 );
            builder.addInput( [ [t], [[t], [t], [t], [t], [t]] ], 0 );
            builder.addInput( [ [t], [[f], [t], [t], [t], [t]] ], 0 );
            builder.addInput( [ [t], [[t], [t], [f], [t], [t]] ], 0 );
            builder.addInput( [ [t, f], [[t, t], [f, t], [t, t], [f, t], [t, t]] ], 0 );
            builder.addInput( [ [t, f], [[t, t], [t, t], [f, t], [t, t], [t, t]] ], 1 );
        } );
    });

    test(ReferenceImplementations.countExamReady, function (builder) {
        withClassroomFormatter( builder, function (builder) {
            var t = true;
            var f = false;
            
            builder.addInput( [ [t, t], [[f, t], [t, t], [t, t], [t, t], [t, t]] ] );
            builder.addInput( [ [t,t,t,t], [[t,t,t,t],[t,t,t,t],[t,t,t,t],[t,t,t,t],[t,t,t,t]] ] );
            builder.addInput( [ [t,f,t,t], [[t,t,t,t],[t,t,t,t],[t,t,t,t],[t,t,t,t],[t,t,t,t]] ] );
            builder.addInput( [ [t,t,f,t], [[t,t,t,t],[t,t,t,t],[t,t,t,t],[t,t,t,t],[t,t,t,t]] ] );
            builder.addInput( [ [t,t,t,t], [[f,t,t,t],[t,f,t,t],[f,t,f,t],[t,t,t,f],[t,t,t,f]] ] );
        } );
    });

    test(ReferenceImplementations.unpoweredRows, function (builder) {
        withClassroomFormatter( builder, function (builder) {
            var t = true;
            var f = false;
            
            builder.addInput( [ [t, t], [[t, t]] ] );
            builder.addInput( [ [t, t], [[f, f]] ] );
            builder.addInput( [ [t, f], [[t, t]] ] );
            builder.addInput( [ [f, t], [[t, t]] ] );
            builder.addInput( [ [f, f], [[t, t]] ] );
            builder.addInput( [ [t, f, t, f], [[t, t, t, t]] ] );
        } );
    });

    test(ReferenceImplementations.stringOfUnpoweredRows, function (builder) {
        withClassroomFormatter( builder, function (builder) {
            var t = true;
            var f = false;
            
            builder.addInput( [ [t, t], [[t, t]] ] );
            builder.addInput( [ [t, t], [[f, f]] ] );
            builder.addInput( [ [t, f], [[t, t]] ] );
            builder.addInput( [ [f, t], [[t, t]] ] );
            builder.addInput( [ [f, f], [[t, t]] ] );
            builder.addInput( [ [t, f, t, f], [[t, t, t, t]] ] );
            builder.addInput( [ [f, f, t, f], [[t, t, t, t]] ] );
            builder.addInput( [ [f, f, f, f], [[t, t, t, t]] ] );
        } );
    });    
});
