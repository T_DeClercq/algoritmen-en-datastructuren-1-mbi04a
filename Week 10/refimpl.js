var ReferenceImplementations;
(function (ReferenceImplementations) {
    function createPosition(x, y) {
        return [x, y];
    }
    ReferenceImplementations.createPosition = createPosition;

    function x(pos) {
        return pos[0];
    }
    ReferenceImplementations.x = x;

    function y(pos) {
        return pos[1];
    }
    ReferenceImplementations.y = y;

    function stringOfPosition(pos) {
        return "(" + x(pos) + ", " + y(pos) + ")";
    }
    ReferenceImplementations.stringOfPosition = stringOfPosition;

    function createArray(n, x) {
        var xs = new Array(n);

        for (var i = 0; i !== xs.length; ++i) {
            xs[i] = x;
        }

        return xs;
    }
    ReferenceImplementations.createArray = createArray;

    function createGrid(width, height, init) {
        var grid = new Array(width);

        for (var i = 0; i !== grid.length; ++i) {
            grid[i] = createArray(height, init);
        }

        return grid;
    }
    ReferenceImplementations.createGrid = createGrid;

    function width(grid) {
        return grid.length;
    }
    ReferenceImplementations.width = width;

    function height(grid) {
        return grid[0].length;
    }
    ReferenceImplementations.height = height;

    function at(grid, position) {
        return grid[x(position)][y(position)];
    }
    ReferenceImplementations.at = at;

    function setAt(grid, position, val) {
        grid[x(position)][y(position)] = val;
    }
    ReferenceImplementations.setAt = setAt;

    function createCode(row, index) {
        return [row, index];
    }
    ReferenceImplementations.createCode = createCode;

    function row(code) {
        return code[0];
    }
    ReferenceImplementations.row = row;

    function index(code) {
        return code[1];
    }
    ReferenceImplementations.index = index;

    function stringOfCode(code) {
        return row(code) + "-" + index(code);
    }
    ReferenceImplementations.stringOfCode = stringOfCode;

    function computerPosition(code) {
        return createPosition(index(code), row(code));
    }
    ReferenceImplementations.computerPosition = computerPosition;

    function createClassroom(rows, computersPerRow) {
        var connected = createArray(rows, true);
        var working = createGrid(computersPerRow, rows, true);

        return [connected, working];
    }
    ReferenceImplementations.createClassroom = createClassroom;

    function rowCount(classroom) {
        return classroom[0].length;
    }
    ReferenceImplementations.rowCount = rowCount;

    function computersPerRow(classroom) {
        return width(classroom[1]);
    }
    ReferenceImplementations.computersPerRow = computersPerRow;

    function allComputerCodes(classroom) {
        var result = [];

        for (var row = 0; row !== rowCount(classroom); ++row) {
            for (var i = 0; i !== computersPerRow(classroom); ++i) {
                var code = createCode(row, i);

                result.push(code);
            }
        }

        return result;
    }
    ReferenceImplementations.allComputerCodes = allComputerCodes;

    function isPoweredRow(classroom, row) {
        return classroom[0][row];
    }
    ReferenceImplementations.isPoweredRow = isPoweredRow;

    function isBroken(classroom, code) {
        return !at(classroom[1], computerPosition(code));
    }
    ReferenceImplementations.isBroken = isBroken;

    function isOperational(classroom, code) {
        return isPoweredRow(classroom, row(code)) && !isBroken(classroom, code);
    }
    ReferenceImplementations.isOperational = isOperational;

    function setRowElectricity(classroom, row, electricity) {
        classroom[0][row] = electricity;
    }
    ReferenceImplementations.setRowElectricity = setRowElectricity;

    function setComputerState(classroom, code, working) {
        setAt(classroom[1], computerPosition(code), working);
    }
    ReferenceImplementations.setComputerState = setComputerState;

    function countOperational(classroom) {
        var count = 0;

        for (var row = 0; row !== rowCount(classroom); ++row) {
            for (var i = 0; i !== computersPerRow(classroom); ++i) {
                var code = createCode(row, i);

                if (isOperational(classroom, code)) {
                    ++count;
                }
            }
        }

        return count;
    }
    ReferenceImplementations.countOperational = countOperational;

    function countBroken(classroom) {
        var count = 0;

        for (var row = 0; row !== rowCount(classroom); ++row) {
            for (var i = 0; i !== computersPerRow(classroom); ++i) {
                var code = createCode(row, i);

                if (isBroken(classroom, code)) {
                    ++count;
                }
            }
        }

        return count;
    }
    ReferenceImplementations.countBroken = countBroken;

    function countUnpoweredWorking(classroom) {
        var count = 0;

        for (var r = 0; r !== rowCount(classroom); ++r) {
            for (var i = 0; i !== computersPerRow(classroom); ++i) {
                var code = createCode(r, i);

                if (!isBroken(classroom, code) && !isPoweredRow(classroom, row(code))) {
                    ++count;
                }
            }
        }

        return count;
    }
    ReferenceImplementations.countUnpoweredWorking = countUnpoweredWorking;

    function countExamReadyOnRow(classroom, row) {
        var count = 0;
        var i = 0;

        while (i < computersPerRow(classroom)) {
            var code = createCode(row, i);

            if (isOperational(classroom, code)) {
                count++;
                i += 2;
            } else {
                ++i;
            }
        }

        return count;
    }
    ReferenceImplementations.countExamReadyOnRow = countExamReadyOnRow;

    function countExamReady(classroom) {
        var oddTotal = 0;
        var evenTotal = 0;

        for (var i = 0; i < rowCount(classroom); i += 2) {
            evenTotal += countExamReadyOnRow(classroom, i);
        }

        for (var i = 1; i < rowCount(classroom); i += 2) {
            oddTotal += countExamReadyOnRow(classroom, i);
        }

        return Math.max(evenTotal, oddTotal);
    }
    ReferenceImplementations.countExamReady = countExamReady;

    function classifyComputers(classroom) {
        var operational = [];
        var broken = [];
        var withoutElectricity = [];

        for (var row = 0; row !== rowCount(classroom); ++row) {
            for (var i = 0; i !== computersPerRow(classroom); ++i) {
                var code = createCode(row, i);

                if (isOperational(classroom, code)) {
                    operational.push(code);
                } else if (isBroken(classroom, code)) {
                    broken.push(code);
                } else {
                    withoutElectricity.push(code);
                }
            }
        }

        return [operational, withoutElectricity, broken];
    }
    ReferenceImplementations.classifyComputers = classifyComputers;

    function findRearrangement(classroom) {
        var codes = allComputerCodes(classroom);
        var i = 0;
        var j = codes.length - 1;

        while (i < j) {
            var c1 = codes[i];
            var c2 = codes[j];

            if (!isPoweredRow(classroom, row(c1)) || isOperational(classroom, c1)) {
                ++i;
            } else if (isOperational(classroom, c2)) {
                --j;
            } else if (isBroken(classroom, c2)) {
                --j;
            } else {
                setComputerState(classroom, c1, true);
                setComputerState(classroom, c2, false);
                ++i;
                --j;
            }
        }
    }
    ReferenceImplementations.findRearrangement = findRearrangement;

    function unpoweredRows(classroom) {
        var result = [];

        for (var i = 0; i !== rowCount(classroom); ++i) {
            if (!isPoweredRow(classroom, i)) {
                result.push(i);
            }
        }

        return result;
    }
    ReferenceImplementations.unpoweredRows = unpoweredRows;

    function stringOfUnpoweredRows(classroom) {
        var rows = unpoweredRows(classroom);

        if (rows.length === 0) {
            return "geen";
        } else if (rows.length === 1) {
            return rows[0].toString();
        } else {
            var result = "";

            for (var i = 0; i < rows.length - 1; ++i) {
                if (i !== 0) {
                    result += ", ";
                }

                result += i;
            }

            result += " en " + rows[rows.length - 1];

            return result;
        }
    }
    ReferenceImplementations.stringOfUnpoweredRows = stringOfUnpoweredRows;
})(ReferenceImplementations || (ReferenceImplementations = {}));
