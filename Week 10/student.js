/*
  Schrijf je code hier
*/
function createPosition(x, y){
	var Position = new Array(x,y);
	return Position;
}
function x(position){
	return position[0];
}
function y(position){
	return position[1];
}
function createArray(n, init){
	var arr = new Array(n)
	for(var i=0;i<arr.length;i++){
		arr[i] = init;
	}
	return arr;
}
function createGrid(width, height, init){
	return createArray(width, createArray(height, init));
}
function width(grid){
	return grid.length;
}
function height(grid){
var height = -1;
	for (var x = 0; x<grid.length; x++){
		if (grid[x].length>height) height=grid[x].length;
	}
	return height;
}
function at(grid, pos){
	return grid[x(pos)][y(pos)];
}
function setAt(grid, pos, jos){
	grid[x(pos)][y(pos)] = jos;
}
function createCode(row, idx){
	return Code = new Array(row, idx);
}
function row(code){
	return code[0];
}
function index(code){
	return code[1];
}
function createClassroom(rows, computersPerRow){
	var elektriciteit = createArray(rows, true);
	var defect = createArray(computersPerRow, createArray(rows, true));
	return Classroom = new Array(elektriciteit,defect);
}
function rowCount(room){
	return room[0].length;
}
function computersPerRow(room){
	return width(room[1]);
}
function computerPosition(code){
	return createPosition(index(code),row(code));
}
function allComputerCodes(classroom){
	var amtRows = rowCount(classroom);
	var compsPerRow = computersPerRow(classroom);
	var compCodes = new Array(amtRows * compsPerRow);
	for (var x = 0; x < compCodes.length; x++){
		for(var y = 0; y < amtRows; y++){
			for(var z = 0; z < compsPerRow; z++){
				compCodes[x] = createCode(y, z);
			}
		}
	}
	return compCodes;
	//FIXME
}
function isPoweredRow(classroom, row){
	return classroom[0][row];
}
function isBroken(classroom, code){
	return !classroom[1][index(code)][row(code)];
}
function isOperational(classroom, code){
	return isPoweredRow(classroom, row(code)) && !isBroken(classroom, code);
}
function setRowElectricity(classroom, row, electricity){
	classroom[0][row] = electricity;
}
function setComputerState(classroom, code, working){
	classroom[1][index(code)][row(code)] = working;
}
function countOperational(classroom){
	var workingcomps = 0;
	for (var x = 0; x < allComputerCodes(classroom).length; x++){
		if (isOperational(classroom, allComputerCodes(classroom)[x])) workingcomps++;
	}
	return workingcomps;
}
function countBroken(classroom){
	return allComputerCodes(classroom).length - countOperational(classroom);
}
function countUnpoweredWorking(classroom){
	var result = 0;
	for (var row = 0; row < rowCount(classroom); row++){
		if (!isPoweredRow(classroom, row)){
			for (var idx = 0; idx < computersPerRow(classroom); idx++){
				if (!isBroken(createCode(idx, row))) result++;
			}
		}
	}
	return result;
}
function unpoweredRows(classroom){
	var result = [];
	for(var row = 0; row < rowCount(classroom); row++){
		if (!isPoweredRow(classroom, row)) result.push(row);
	}
	return result;
}
function stringOfUnpoweredRows(classroom){
	if (unpoweredRows(classroom).length == 0) return "geen";
	var result = "";
	for (var x = 0; x < unpoweredRows(classroom).length; x++){
		result = concat(result, unpoweredRows(classroom)[x].toString());
		if (unpoweredRows(classroom).length - x > 0)
			{result = concat(result, ", ");}
		else if (unpoweredRows(classroom).length - x == 0)
			{result = concat(result, " en ");}
	}
	return result;
}